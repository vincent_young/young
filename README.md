#功能介绍
* 容联.云通讯 - 短信
* 短信网
* 支付宝支付【APP支付、异步通知、交易查询、交易退款】
* 微信支付【APP支付、异步通知、交易查询、交易退款、企业支付、现金红包】
* 推送【极光推送】
##### 容联短信
* $AccountSid 开发者主账户ACCOUNT SID
* $Token 账户授权令牌
* $AppId 应用Id
* $to 短信接收端手机号码集合，用英文逗号分开，每批发送的手机号数量不得超过200个
* $options 内容数据外层节点，模板如果没有变量，此参数可不传
* $tempId 模板Id
#####
    (new RL($AccountSid, $Token, $AppId))->sms($to, $options = [], $tempId);
    
##### 短信网
* $Account 用户账号
* $ApiPassword 登陆web平台：基本资料中的接口密码
* $Sign 短信签名
* $to 手机号码，多个以英文逗号隔开
* $code 验证码
######
    (new DXW($Account, $ApiPassword, $Sign))->sms($to, $code);
##### 支付宝APP支付
    (new Payment)->pay('ali_app', $data);
##### 支付宝异步通知
    (new Payment)->pay('ali_notify', function($data){
        //处理商家业务逻辑
        //....................
        //处理成功
        return true;
    });
##### 支付宝交易查询
    (new Payment)->pay('ali_query', $data);
##### 支付宝交易退款
    (new Payment)->pay('ali_refund', $data);

##### 微信APP支付
    (new Payment)->pay('wx_app', $data);
    
##### 微信企业支付
    (new Payment)->pay('wx_mch_pay', $data);
        
##### 微信现金红包
    (new Payment)->pay('wx_mch_red', $data);
        
##### 微信异步通知
    (new Payment)->pay('wx_notify', function($data){
        //处理商家业务逻辑
        //....................
        //处理成功
        return true;
    });
##### 微信交易查询
    (new Payment)->pay('wx_query', $data);
##### 微信交易退款
    (new Payment)->pay('wx_refund', $data);