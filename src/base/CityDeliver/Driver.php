<?php

namespace young\CityDeliver;

abstract class Driver
{
    protected $gateway;

    abstract public function createOrder(array $data);

    abstract public function queryOrder($order_no);

    abstract public function cancelOrder(array $data);

    abstract public function cancelReasons();

    protected function doPost($url, array $data)
    {
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $headers = array(
          'Content-Type: application/json',
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($curl);
        curl_close($curl);
        return json_decode($res, true);
    }

    protected function getUrl($api)
    {
        return $this->gateway . $api;
    }

    protected function success(array $data)
    {
        return ['status' => 'success', 'data' => $data];
    }

}