<?php

namespace young\CityDeliver\driver;

use young\CityDeliver\Driver;

class Ele extends Driver
{
    private $app_id = '';
    private $secret_key = '';
    private $apis = [
      'createOrder' => '',//新订单
      'queryOrder'  => '/v2/order/query',//查询订单
      'cancelOrder' => ''//取消订单
    ];

    public function __construct($options)
    {
        $this->gateway = 'https://exam-anubis.ele.me/anubis-webapi';
        $this->app_id = isset($options['app_id']) ? $options['app_id'] : '';
        $this->secret_key = isset($options['secret_key']) ? $options['secret_key'] : '';
    }

    public function createOrder(array $data)
    {
        // TODO: Implement createOrder() method.
    }

    public function queryOrder($order_no)
    {
        $body = [
          'app_id'             => $this->app_id,
          'partner_order_code' => $order_no,
          'salt'               => rand(1000, 9999),
          'signature'          => ''
        ];
        $data = $this->request($this->apis['queryOrder'], $body);
        dump($data);
    }

    public function cancelOrder(array $data)
    {
        // TODO: Implement cancelOrder() method.
    }

    //取消原因
    public function cancelReasons()
    {
        $list = [
          ['id' => 1, 'content' => '订单长时间未分配骑手'],
          ['id' => 2, 'content' => '分配骑手后，骑手长时间未取件'],
          ['id' => 3, 'content' => '骑手告知不配送，让取消订单'],
          ['id' => 4, 'content' => '商品缺货/无法出货/已售完'],
          ['id' => 5, 'content' => '商户联系不上门店/门店关门了'],
          ['id' => 6, 'content' => '商户发错单'],
          ['id' => 7, 'content' => '商户/顾客自身定位错误'],
          ['id' => 8, 'content' => '商户改其他第三方配送'],
          ['id' => 9, 'content' => '顾客下错单/临时不想要了'],
          ['id' => 10, 'content' => '顾客自取/不在家/要求另改时间配送'],
        ];
        return $list;
    }

    private function request($api, $data)
    {
        return $this->doPost($this->getUrl($api), $data);
    }
}