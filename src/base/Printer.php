<?php

namespace young;

use young\Printer\driver\Feieyun;
use young\Printer\driver\Printcenter365;

class Printer
{
    public static $instance = [];

    /**
     * @param $channel
     * @param array $dev
     * @return mixed
     * @throws \Exception
     */
    private function connect($channel, array $dev)
    {
        if (is_null($dev) || !is_array($dev)) {
            throw new \Exception($channel . '请传入开发者信息');
        }

        if (!isset(self::$instance[$channel])) {
            $class = '\\young\\Printer\\driver\\' . ucwords($channel);
            self::$instance[$channel] = new $class($dev);
        }

        return self::$instance[$channel];

    }

    /**
     * 飞鹅云
     * @param array $dev
     * @return Feieyun
     */
    public static function feieyun(array $dev = [])
    {
        $dev = ['user' => 'xiongwq@foxmail.com', 'ukey' => 'ZS3KJMxZcEPzEABx'];
        return self::connect('feieyun', $dev);
    }

    /**
     * Printcenter 365
     * @param array $dev
     * @return Printcenter365
     */
    public static function Printcenter365(array $dev = [])
    {
        return self::connect('printercenter', $dev);
    }

}