<?php
/**
 * @desc 支付宝.交易查询
 * @document https://docs.open.alipay.com/api_1/alipay.trade.query/
 * @author Vincent
 */

namespace young\payment;

use young\Common;

class AliQuery extends AliConfig
{
    //交易状态
    private $tradeStatus = [
        'WAIT_BUYER_PAY' => '交易创建，等待买家付款',
        'TRADE_CLOSED' => '未付款交易超时关闭，或支付完成后全额退款',
        'TRADE_SUCCESS' => '交易支付成功',
        'TRADE_FINISHED' => '交易结束，不可退款'
    ];

    public function run($data)
    {
        $this->setConfig($data['config']);
        return $this->handle($data['order_sn']);
    }

    /**
     * @param $orderSn 商户订单号
     * @return bool
     * @throws \Exception
     */
    private function handle($orderSn)
    {
        $common = [];
        $common['app_id'] = $this->appId;
        $common['method'] = 'alipay.trade.query';
        $common['format'] = self::FORMAT;
        $common['charset'] = self::CHARSET;
        $common['sign_type'] = self::SIGN_TYPE;
        $common['timestamp'] = date('Y-m-d H:i:s', time());
        $common['version'] = self::VERSION;
        $common['biz_content'] = [];
        $common['biz_content']['out_trade_no'] = $orderSn;
        $common['biz_content'] = json_encode($common['biz_content'], JSON_UNESCAPED_UNICODE);
        $common = array_filter($common);

        ksort($common);

        $rsa2 = new AliRsa2;

        //签名
        $rsa2->setKey($rsa2->getRsaKeyValue($this->rsaPrivateKey, 'private'));
        $common['sign'] = $rsa2->encrypt(urldecode(http_build_query($common)));
        $common = $rsa2->splicingString($common);
        $result = Common::request_get(self::GETEWAY_URL . '?' . $common);

        $result = json_decode($result, true);
        $data = $result['alipay_trade_query_response'];

        //验证签名
        $rsa2->setKey($rsa2->getRsaKeyValue($this->aliPublicKey, 'public'));
        $verifySign = $rsa2->rsaVerify(json_encode($data, JSON_UNESCAPED_UNICODE), $result['sign']);

        if ($verifySign === true) {
            if ($data['code'] == '10000') {
                return [
                    'error' => false,
                    'order_sn' => $data['out_trade_no'],
                    'trade_no' => $data['trade_no'],
                    'total_amount' => $data['total_amount'],//交易总金额
                    'pay_date' => $data['send_pay_date'],
                    'trade_status' => $this->tradeStatus[$data['trade_status']]
                ];
            } else {
                throw new \Exception('[code:' . $data['code'] . '][msg:' . $data['msg'] . ']');
            }
        } else {
            throw new \Exception('签名失败');
        }
    }
}