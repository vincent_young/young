<?php

namespace young\payment;

use young\Common;

class WxConfig
{
    public $appid = '';
    public $mch_id = '';
    public $key = '';//key设置路径：微信商户平台(pay.weixin.qq.com)-->账户设置-->API安全-->密钥设置
    public $apiclient_cert = '';
    public $apiclient_key = '';
    public $rootca = '';

    const SIGN_TYPE = 'MD5';

    public function setConfig($options = [])
    {
        if ($options && is_array($options) && !empty($options)) {
            isset($options['appid']) && ($this->appid = $options['appid']);
            isset($options['mch_id']) && ($this->mch_id = $options['mch_id']);
            isset($options['key']) && ($this->key = $options['key']);
            isset($options['apiclient_cert']) && ($this->apiclient_cert = $options['apiclient_cert']);
            isset($options['apiclient_key']) && ($this->apiclient_key = $options['apiclient_key']);
            isset($options['krootcaey']) && ($this->rootca = $options['rootca']);
        }
        return $this;
    }

    //金额元单位转换成分单位
    public function formatAmount($amount)
    {
        return $amount * 100;
    }

    //签名
    public function sign($data)
    {
        $data = array_filter($data);
        ksort($data);
        return strtoupper(md5(urldecode(http_build_query($data)) . '&key=' . $this->key));
    }

    //验证签名
    public function verifySign($data)
    {
        if (isset($data['sign'])) {
            $sign = $data['sign'];
            unset($data['sign']);
            if ($this->sign($data) == $sign) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //返回结果给微信
    public function returnResult($code, $msg)
    {
        $data = [
          'return_code' => $code,
          'return_msg'  => $msg
        ];
        return Common::arrayToXml($data);
    }

    public function request_post($url = '', $request_data = [])
    {
        $basePath = dirname(__FILE__) . '/cert/';
        $apiclient_cert = $basePath . 'apiclient_cert_' . md5($this->appid) . '.pem';
        $apiclient_key = $basePath . 'apiclient_key_' . md5($this->appid) . '.pem';
        $rootca = $basePath . 'rootca_' . md5($this->appid) . '.pem';
        file_put_contents($apiclient_cert, $this->apiclient_cert);
        file_put_contents($apiclient_key, $this->apiclient_key);
        file_put_contents($rootca, $this->rootca);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSLCERT, $apiclient_cert);
        curl_setopt($ch, CURLOPT_SSLKEY, $apiclient_key);
        curl_setopt($ch, CURLOPT_CAINFO, $rootca);
        $output = curl_exec($ch);
        curl_close($ch);
        @unlink($apiclient_cert);
        @unlink($apiclient_key);
        @unlink($rootca);
        return $output;
    }
}