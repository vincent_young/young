<?php

namespace young\payment;

class AliConfig
{
    public $appId = '';//支付宝分配给开发者的应用Id
    public $sellerId = '';//收款支付宝账号对应的支付宝唯一用户号。以2088开头的纯16位数字

    //支付宝公钥
    public $aliPublicKey = '';

    //商户私钥
    public $rsaPrivateKey = '';

    const CHARSET = 'UTF-8';
    const FORMAT = 'JSON';
    const VERSION = '1.0';
    const SIGN_TYPE = 'RSA2';
    const GETEWAY_URL = 'https://openapi.alipay.com/gateway.do';

    public function setConfig($options = [])
    {
        if ($options && is_array($options) && !empty($options)) {
            isset($options['appId']) && ($this->appId = $options['appId']);
            isset($options['sellerId']) && ($this->sellerId = $options['sellerId']);
            isset($options['aliPublicKey']) && ($this->aliPublicKey = $options['aliPublicKey']);
            isset($options['rsaPrivateKey']) && ($this->rsaPrivateKey = $options['rsaPrivateKey']);
        }
        return $this;
    }

}