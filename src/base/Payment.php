<?php

namespace young;

use Exception;
use young\payment\AliAppPay;
use young\payment\AliRefund;
use young\payment\AliQuery;
use young\payment\AliFundTransQuery;
use young\payment\AliNotify;
use young\payment\AliWapPay;
use young\payment\AliWebPay;
use young\payment\AliCompanyPay;
use young\payment\WxAppPay;
use young\payment\WxNotify;
use young\payment\WxQuery;
use young\payment\WxRefund;
use young\payment\WxMchPay;
use young\payment\WxMchRed;

class Payment
{

    const ALI_CHANNEL_APP = 'ali_app';// 支付宝 APP 支付
    const ALI_CHANNEL_WAP = 'ali_wap';// 支付宝手机网页支付
    const ALI_CHANNEL_WEB = 'ali_web';// 支付宝电脑网站支付
    const ALI_CHANNEL_QR = 'ali_qr';// 支付宝当面付-扫码支付
    const ALI_CHANNEL_BAR = 'ali_bar';// 支付宝当面付-条码支付
    const ALI_CHANNEL_COMPANY = 'ali_company';// 支付宝企业支付到账户

    const ALI_CHANNEL_REFUND = 'ali_refund';//支付宝退款
    const ALI_CHANNEL_QUERY = 'ali_query';//支付宝交易查询
    const ALI_CHANNEL_FUND_TRANS_QUERY = 'ali_fund_trans_query';//支付宝交易查询alipay.fund.trans.order.query
    const ALI_CHANNEL_NOTIFY = 'ali_notify';//支付宝支付异步通知

    const WX_CHANNEL_APP = 'wx_app';// 微信 APP 支付
    const WX_CHANNEL_PUB = 'wx_pub';// 微信公众号支付
    const WX_CHANNEL_QR = 'wx_qr';// 微信公众号扫码支付
    const WX_CHANNEL_BAR = 'wx_bar';// 微信刷卡支付
    const WX_CHANNEL_WAP = 'wx_wap';// 微信 WAP 支付（此渠道仅针对特定客户开放）
    const WX_CHANNEL_LITE = 'wx_lite';// 微信小程序支付
    const WX_MCH_PAY = 'wx_mch_pay';// 微信商户支付
    const WX_MCH_RED = 'wx_mch_red';// 微信现金红包

    const WX_CHANNEL_NOTIFY = 'wx_notify';//微信支付异步通知
    const WX_CHANNEL_QUERY = 'wx_query';//微信支付查询
    const WX_CHANNEL_REFUND = 'wx_refund';//微信支付退款

    public function pay($channel, $data)
    {
        try {
            switch ($channel) {
                case self::ALI_CHANNEL_APP:
                    return (new AliAppPay)->run($data);//支付宝APP支付
                    break;
                case self::ALI_CHANNEL_WAP:
                    return (new AliWapPay)->run($data);//支付宝手机网页支付
                    break;
                case self::ALI_CHANNEL_WEB:
                    return (new AliWebPay)->run($data);//支付宝电脑网站支付
                    break;
                case self::ALI_CHANNEL_COMPANY:
                    return (new AliCompanyPay)->run($data);//支付宝企业支付到个人账户
                    break;
                case self::WX_CHANNEL_APP:
                    return (new WxAppPay)->run($data);//微信APP支付
                    break;
                case self::WX_MCH_PAY:
                    return (new WxMchPay)->run($data);//微信商户支付
                    break;
                case self::WX_MCH_RED:
                    return (new WxMchRed)->run($data);//微信现金红包
                    break;
                default:
                    throw new Exception('暂不支持『' . $channel . '』');
                    break;
            }
        } catch (Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function refund($channel, $data)
    {
        try {
            switch ($channel) {
                case self::ALI_CHANNEL_REFUND:
                    return (new AliRefund)->run($data);//支付宝退款
                    break;
                case self::WX_CHANNEL_REFUND:
                    return (new WxRefund)->run($data);//微信退款
                    break;
                default:
                    throw new Exception('暂不支持『' . $channel . '』');
                    break;
            }
        } catch (Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function query($channel, $data)
    {
        try {
            switch ($channel) {
                case self::ALI_CHANNEL_QUERY:
                    return (new AliQuery)->run($data);//支付宝交易查询
                    break;
                case self::ALI_CHANNEL_FUND_TRANS_QUERY:
                    return (new AliFundTransQuery)->run($data);//支付宝转账交易查询
                    break;
                case self::WX_CHANNEL_QUERY:
                    return (new WxQuery)->run($data);//微信支付查询
                    break;
                default:
                    throw new Exception('暂不支持『' . $channel . '』');
                    break;
            }
        } catch (Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function notify($channel, $config = [], $callback)
    {
        try {
            switch ($channel) {
                case self::ALI_CHANNEL_NOTIFY:
                    return (new AliNotify)->setConfig($config)->run($callback);//支付宝支付异步通知
                    break;
                case self::WX_CHANNEL_NOTIFY:
                    return (new WxNotify)->setConfig($config)->run($callback);//微信支付异步通知
                    break;
                default:
                    throw new Exception('暂不支持『' . $channel . '』');
                    break;
            }
        } catch (Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }
}