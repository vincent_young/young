<?php

namespace young;

use young\CityDeliver\Driver;

class CityDeliver
{
    public static $instance = [];
    public static $config = null;

    public static function setConfig(array $config)
    {
        self::$config = $config;
    }

    private function connect($channel)
    {
        if (is_null(self::$config)) {
            throw new \Exception('请先执行setConfig');
        } elseif (!isset(self::$config[$channel])) {
            throw new \Exception($channel . '未开通');
        } else {
            $options = self::$config[$channel];
        }

        if (!isset(self::$instance[$channel])) {
            $class = '\\young\\CityDeliver\\driver\\' . ucwords($channel);
            self::$instance[$channel] = new $class($options);
        }

        return self::$instance[$channel];

    }

    public static function store($channel = '')
    {
        return self::connect($channel);
    }

}