<?php
/**
 * @author Vincent
 * @desc 短信网
 * @document http://www.duanxinwang.cc/html/apidocs.html#section-5
 */

namespace young;

class DXW
{
    private $Account;
    private $ApiPassword;
    private $Sign;
    const API_URL = 'http://web.duanxinwang.cc/asmx/smsservice.aspx?';

    private static $resultCode = [
        0 => '提交成功',
        1 => '含有敏感词汇',
        2 => '含有敏感词汇',
        3 => '没有号码',
        4 => '包含sql语句',
        10 => '账号不存在',
        11 => '账号注销',
        12 => '账号停用',
        13 => 'IP鉴权失败',
        14 => '格式错误',
        '-1' => '系统异常'
    ];

    /**
     * DXW constructor.
     * @param $Account 用户账号
     * @param $ApiPassword 登陆web平台：基本资料中的接口密码
     * @param $Sign 短信签名
     */
    public function __construct($Account, $ApiPassword, $Sign)
    {
        $this->Account = $Account;
        $this->ApiPassword = $ApiPassword;
        $this->Sign = $Sign;
    }

    /**
     * @param $to 手机号码，多个以英文逗号隔开
     * @param $code 验证码
     * @return bool|mixed
     */
    public function sms($to, $code)
    {
        header("Content-Type: text/html; charset=UTF-8");
        $flag = 0;
        $params = '';
        $argv = array(
            'name' => $this->Account,
            'pwd' => $this->ApiPassword,
            'content' => '您的短信验证码为：' . $code . '，请勿将验证码提供给他人，5分钟内有效。',
            'mobile' => $to,
            'stime' => '',
            'sign' => $this->Sign,
            'type' => 'pt',
            'extno' => ''
        );
        foreach ($argv as $key => $value) {
            if ($flag != 0) {
                $params .= "&";
                $flag = 1;
            }
            $params .= $key . "=";
            $params .= urlencode($value);
            $flag = 1;
        }
        $url = self::API_URL . $params;
        $con = substr(file_get_contents($url), 0, 1);
        if ($con == '0') {
            return true;
        } else {
            return self::$resultCode[$con];
        }
    }
}