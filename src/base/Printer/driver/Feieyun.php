<?php

namespace young\Printer\driver;

use young\Printer\Driver;

class Feieyun implements Driver
{
    use FeieyunTemplate;

    private $user = '';
    private $ukey = '';

    public function __construct($dev)
    {
        $this->user = $dev['user'];
        $this->ukey = $dev['ukey'];
    }

    public function gateway()
    {
        return 'https://api.feieyun.cn/Api/Open/';
    }

    /**
     * @param array $printer
     * @printer device_no
     * @printer device_key
     * @printer device_name
     * @return mixed
     */
    public function printerAdd(array $printer)
    {
        $body = [
          'printerContent' => vsprintf('%s # %s # %s', [$printer['device_no'], $printer['device_key'], $printer['device_name']])
        ];
        $data = $this->request('Open_printerAddlist', $body);
        if (empty($data['ok'])) {
            $this->fail($data['no'][0]);
        } else {
            return $this->success('添加成功');
        }
    }

    public function printerDel($device_no)
    {
        $body = [
          'snlist' => $device_no
        ];
        $data = $this->request('Open_printerDelList', $body);
        if (empty($data['ok'])) {
            $this->fail($data['no'][0]);
        } else {
            return $this->success('删除成功');
        }
    }

    public function getPrinterStatus($device_no)
    {
        $body = [
          'sn' => $device_no
        ];
        $data = $this->request('Open_queryPrinterStatus', $body);
        return $data;
    }

    /**
     * @param array $order
     * @order device_no
     * @order content
     * @order times 默认1
     * @return mixed
     */
    public function pushPrint(array $order)
    {
        $body = [
          'sn'      => $order['device_no'],
          'content' => $order['content'],
          'times'   => (isset($order['times']) && $order['times'] > 0) ? $order['times'] : 1
        ];
        $data = $this->request('Open_printMsg', $body);
        return $this->success($data);
    }

    public function success($data)
    {
        return $data;
    }

    public function fail($message)
    {
        throw new \Exception($message);
    }

    private function request($api, $body)
    {

        $time = time();
        $data = [
          'user'    => $this->user,
          'stime'   => $time,
          'sig'     => $this->sign($time),
          'apiname' => $api
        ];
        $data = http_build_query(array_merge($data, $body));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->gateway());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $output = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($output, true);
        if (!isset($result['ret'])) {
            $this->fail('接口异常');
        } elseif ($result['ret'] !== 0) {
            $this->fail($result['msg']);
        } else {
            return $result['data'];
        }
    }

    private function sign($time)
    {
        return sha1($this->user . $this->ukey . $time);
    }
}