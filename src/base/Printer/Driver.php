<?php

namespace young\Printer;

interface Driver
{
    public function gateway();

    public function success($data);

    public function fail($message);

}